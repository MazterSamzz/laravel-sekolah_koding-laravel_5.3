<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>@yield('title')</title>
</head>
<body>
    <header>
        <nav>
            <a href="/">Home</a>
            <a href="/blog">Blog</a>
        </nav>
    </header>
    @yield('content')
</body>
<footer>
    <?php $year = date('Y'); ?>
    <p>&copy; Laravel & MazterSamzz 2021{{ $year>2021 ? '-'.$year : '' }}</p>
</footer>
</html>